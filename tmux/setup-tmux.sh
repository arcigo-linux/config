#!/bin/bash

# Title	   	   : Tmux Setup
# Purpose	   : To setup tmux automatically
# Author	   : Henry
# Usage		   : ./tmux-setup.sh
# Version	   : 0.4
# Created Date : 29/10/2022
# Modified Date: 30/10/2022
# Info		   : Configured to use. If there is any problem, kindly open a issue.

# Package Manager varies according to their distros
# Here is the list:
# 	Debian	- apt-get
# 	Fedora	- dnf
# 	Arch	- pacman
# 	Red-hat	- yum
# 	Opensuse- zypper

PKGMGR="pacman -Syy --noconfirm"
DEPENDENCIES="tmux git wget"
TMUX_CONFIG="${HOME}/.tmux.conf"

# START #
# Error and warnings
function _red_text(){
	QUERY="$*"
	echo -e "\e[31m${QUERY}\e[0m"
}

# Successful output
function _green_text(){
	QUERY="$*"
	echo -e "\e[32m${QUERY}\e[0m"
}

# Informative
function _yellow_text(){
	QUERY="$*"
	echo -e "\e[33m${QUERY}\e[0m"
}

# Show process
function _blue_text(){
	QUERY="$*"
	echo -e "\e[34m${QUERY}\e[0m"
}

# If there is any dependencies, install them !
function install_dependencies(){
	_green_text [+] Installing dependencies...
	sudo ${PKGMGR} ${DEPENDENCIES} || panic
}

## Download & install Configuration
function download_config(){
	CONFIG="https://gitlab.com/arcigo-linux/config/-/raw/main/tmux/tmux.conf"
	#if [[ -e ${TMUX_CONFIG} ]]; then
	#	_yellow_text [*] Config already exist !
	#	_blue_text [*] Backing up tmux config...
	#	mv ${TMUX_CONFIG} ${HOME}/.tmux.conf.bak
	#	_green_text [+] Tmux config backed up successfully !
	#fi
	
	# Download and install config
	if [[ -f ./tmux.conf ]]; then
		cp tmux.conf ${TMUX_CONFIG}
	else
		_green_text [*] Downloading Tmux config...
		wget -c ${CONFIG} || panic
		cp tmux.conf ${TMUX_CONFIG}
	fi
	_green_text [+] Tmux config installed successfully...
	sleep 2
}

## Disclaimer
function disclaimer(){
	_red_text [!] DISCLAIMER:
	_red_text \\tThis script will wipe the configs of tmux !
	_red_text \\tWant to continue? Make sure to backup your config !
	read -p "[?] Press [Enter] to continue..."
}

## Install Tmux Plugin Manager
function install_tpm(){
	PLUGIN_REPO="https://github.com/tmux-plugins/tpm"
	_blue_text [*] Installing Tmux plugin manager...
	git clone ${PLUGIN_REPO} ${HOME}/.tmux/plugins/tpm 2>/dev/null || panic
	_green_text [+] Tmux plugin manager installed successfully...
	sleep 2
}

## Configure Tmux to use
function configure_tmux(){
	_blue_text [*] Creating new tmux session...
	tmux new-session -d -s foobar && sleep 2
	_blue_text [*] Reloading the tmux config...
	tmux source ${TMUX_CONFIG} && sleep 1
	_green_text [+] Installing Tmux plugins...
	${HOME}/.tmux/plugins/tpm/scripts/install_plugins.sh 2>/dev/null || panic && sleep 2
	if [ $? == 0 ]; then
		_green_text [+] Tmux plugins installed successfully...
	fi
	_yellow_text [*] Killing tmux server...
	tmux kill-server && sleep 2
	_green_text [!] Tmux is now fully configured !
	_green_text [!] Use \"tmux new -s \<session-name\>\"
}

## Check for old config
function remove_if_config_exists(){
	disclaimer
	if [ -e ${TMUX_CONFIG} ]; then
		_yellow_text [*] Removing Tmux config...
		rm  ${TMUX_CONFIG} && sleep 1
	fi
	if [ -d "${HOME}/.tmux/plugins/" ]; then
		_yellow_text [*] Removing the already installed Tmux plugins...
		rm -rf  ${HOME}/.tmux/ && sleep 1
	fi
}

# Panic if something goes wrong; we can use this is
# in script with the || syntax, if the command fails
function panic(){
	_red_text [-] Fatal error, aborting...
	exit -1
}

# Make sure that the user is not running this as root
# Info: If you want to run this as root, then change the operator '==' to '!='
function verify_root(){
	if [ "$UID" == 0 ]; then
		_red_text [$0]: Don\'t run this script as root !
		# _red_text [$0]: You must be root to run this script !
		exit -1
	fi
}

# It is the main function, calls other functions
function main(){
	verify_root
	_blue_text [+] Starting Tmux Setup... && sleep 2
	remove_if_config_exists
	install_dependencies
	download_config
	install_tpm
	configure_tmux
	exit 0
}

# Calling main function, passing in all passed arguments to every function
main "$@"

# END #
